//libs
import 'angular-animate';
import 'angular-meteor';
import 'angular-sanitize';
import 'angular-ui-router';
import 'ionic-scripts';
import Angular from 'angular';
import { Meteor } from 'meteor/meteor';

// modules

const App = 'chat app';

//app
Angular.module(App, [
	'angular-meteor',
	'ionic'
]);

//startup

if(Meteor.isCordova) {
	Angular.element(document).on('deviceready', onReady);
}
else {
Angular.element(document).ready(onReady);
}

function onReady(){
	Angula.bootstrap(document, [App]);
}

